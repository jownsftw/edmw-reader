//
//  EDMWAppDelegate.h
//  EDMW Got-App-Got-Talk!
//
//  Created by A Jekanath on 1/8/14.
//  Copyright (c) 2014 LiteralDev Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EDMWAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
