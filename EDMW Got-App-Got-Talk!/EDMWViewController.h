//
//  EDMWViewController.h
//  EDMW Got-App-Got-Talk!
//
//  Created by A Jekanath on 1/8/14.
//  Copyright (c) 2014 LiteralDev Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TFHpple.h"
#import "EDMWThreadTableViewController.h"
#import "MMProgressHUD.h"
@interface EDMWViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)refreshButtonClicked:(id)sender;

@end
