//
//  main.m
//  EDMW Got-App-Got-Talk!
//
//  Created by A Jekanath on 1/8/14.
//  Copyright (c) 2014 LiteralDev Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EDMWAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([EDMWAppDelegate class]));
    }
}
