//
//  EDMWViewController.m
//  EDMW Got-App-Got-Talk!
//
//  Created by A Jekanath on 1/8/14.
//  Copyright (c) 2014 LiteralDev Studios. All rights reserved.
//

#import "EDMWViewController.h"

@interface EDMWViewController ()
{
    NSMutableArray *threadTitleData;
    NSMutableArray *threadURLData;
    NSMutableArray *threadTSData;
    NSMutableArray *threadLastPostTimeData;
    NSMutableArray *threadLastPosterData;
    NSMutableArray *threadLastPostDateData;
    NSMutableArray *threadRepliesData;
    BOOL threadPromoted;
}
@end

@implementation EDMWViewController
@synthesize tableView;
- (void)viewDidLoad
{
    [super viewDidLoad];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

	// Do any additional setup after loading the view, typically from a nib.
    self.navigationController.navigationBar.barTintColor =  [UIColor colorWithRed:4/255.0f green:205/255.0f blue:150/255.0f alpha:1.0f];
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont
                                                                           fontWithName:@"HelveticaNeue" size:14], NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    

    threadTitleData = [[NSMutableArray alloc]init];
    threadURLData = [[NSMutableArray alloc]init];
    threadTSData = [[NSMutableArray alloc]init];
    threadLastPostTimeData = [[NSMutableArray alloc]init];
    threadLastPosterData = [[NSMutableArray alloc]init];
    threadLastPostDateData = [[NSMutableArray alloc]init];
    threadRepliesData = [[NSMutableArray alloc]init];
    threadPromoted = FALSE;
    
    [self loadThreads];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadThreads
{
    NSData *result = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:@"http://forums.hardwarezone.com.sg/eat-drink-man-woman-16/"]];
    TFHpple *xpath = [[TFHpple alloc] initWithHTMLData:result];
    int threadsToIgnore = 0;
    //use xpath to search element
    NSArray *ignoredata = [xpath searchWithXPathQuery:@"//tr[@class='hwz-sticky']/td[@class='alt1']/div/a"];
    for (TFHppleElement *item in ignoredata)
    {   //write log
        threadsToIgnore++;
    }
    NSArray *promotedThread = [xpath searchWithXPathQuery:@"//tr[@class='hwz-promoted']/td[@class='alt1']/div/a"];
    for (TFHppleElement *item in promotedThread)
    {   //write log
        threadPromoted = TRUE;
    }
    /////
    
    
    NSArray *data = [xpath searchWithXPathQuery:@"//*[@id=\"threadbits_forum_16\"]/tr/td[@class='alt1']/div/a"];
    int itemCount = threadsToIgnore;

    for (TFHppleElement *item in data)
    {   //write log
        if(itemCount == 0){
            if(item.firstChild.content){
        [threadURLData addObject:[item.attributes valueForKey:@"href"]];
        [threadTitleData addObject:item.firstChild.content];
            }
        }
        else
            itemCount--;
    }
    
    NSArray *posterdata = [xpath searchWithXPathQuery:@"//*[@id=\"threadbits_forum_16\"]/tr/td[@class='alt1']/div[@class='smallfont']/span"];
    int posterCount = threadsToIgnore - 1;
    for (TFHppleElement *item in posterdata)
    {   //write log
        if(posterCount == 0){
            if(item.firstChild.content)
            [threadTSData addObject:item.firstChild.content];
        }
        else
            posterCount--;
    }
    
    NSArray *lastPostTimeData = [xpath searchWithXPathQuery:@"//*[@id=\"threadbits_forum_16\"]/tr/td[@class='alt2']/div[@class='smallfont']/span[@class='time']"];
    int lastPostCount = threadsToIgnore - 1;
    for (TFHppleElement *item in lastPostTimeData)
    {   //write log
        if(lastPostCount == 0){
            if(item.firstChild.content)
                //[threadTSData addObject:item.firstChild.content];
                [threadLastPostTimeData addObject:item.firstTextChild.content];
        }
        else
            lastPostCount--;
    }
    //Having an issue//
    
    /*
    NSArray *lastPostDateData = [xpath searchWithXPathQuery:@"//*[@id=\"threadbits_forum_16\"]/tr/td[@class='alt2']/div[@class='smallfont']"];
    int lastPostDateCount = threadsToIgnore;
    for (TFHppleElement *item in lastPostDateData)
    {   //write log
        if(lastPostDateCount == 0){
            if(item.firstChild.content)
                [threadLastPostDateData addObject:item.firstChild.content];
        }
        else
            lastPostDateCount--;
    }
    */
    
    NSArray *threadPostData = [xpath searchWithXPathQuery:@"//*[@id=\"threadbits_forum_16\"]/tr/td[@class='alt2']/div[@class='smallfont']/a"];
    int threadPostcount = threadsToIgnore * 2 - 2;
    for (TFHppleElement *item in threadPostData)
    {   //write log
        if(threadPostcount == 0){
            if(item.firstChild.content)
                [threadLastPosterData addObject:item.firstChild.content];
        }
        else
            threadPostcount--;
    }
    //*[@id="threadbits_forum_16"]/tr[10]/td[5]
    NSArray *repliesPostData = [xpath searchWithXPathQuery:@"//*[@id=\"threadbits_forum_16\"]/tr/td[@align='center']/a"];
    int repliesPostCount = threadsToIgnore - 1;
    for (TFHppleElement *item in repliesPostData)
    {   //write log
        if(repliesPostCount == 0){
            if(item.firstChild.content)
                [threadRepliesData addObject:item.firstChild.content];
        }
        else
            repliesPostCount--;
    }
    
    [self.tableView reloadData];
    [MMProgressHUD dismiss];

}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{    
    return [threadTitleData count];    //count number of row from counting array hear cataGorry is An Array
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"Cell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    cell.backgroundColor = [UIColor whiteColor];
    if(indexPath.row == 0){
        if(threadPromoted)
            cell.backgroundColor = [UIColor colorWithRed:255/255.0f green:255/255.0f blue:154/255.0f alpha:1.0f];
    }
 
    cell.textLabel.text = [threadTitleData objectAtIndex:indexPath.row];
    cell.textLabel.textColor = [UIColor colorWithRed:70/255.0f green:44/255.0f blue:117/255.0f alpha:1.0f];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Last Post: %@ by %@ | %@ replies",[threadLastPostTimeData objectAtIndex:indexPath.row],[threadLastPosterData objectAtIndex:indexPath.row],[threadRepliesData objectAtIndex:indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    EDMWThreadTableViewController *vc = (EDMWThreadTableViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"EDMWThread"];
    vc.threadURL = [threadURLData objectAtIndex:indexPath.row];
    vc.threadTitle = [threadTitleData objectAtIndex:indexPath.row];
    vc.hidesBottomBarWhenPushed = TRUE;
    [self.navigationController pushViewController:vc animated:YES];
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];


}

- (IBAction)refreshButtonClicked:(id)sender {
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleFade];
    [MMProgressHUD showWithStatus:@"Refreshing..."];
    
    [threadTitleData removeAllObjects];
    [threadURLData removeAllObjects];
    [threadTSData removeAllObjects];
    [threadLastPostTimeData removeAllObjects];
    [threadLastPosterData removeAllObjects];
    [threadLastPostDateData removeAllObjects];
    [threadRepliesData removeAllObjects];
    [self.tableView reloadData];
    [self loadThreads];
}
@end
