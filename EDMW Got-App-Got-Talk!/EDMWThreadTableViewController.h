//
//  EDMWThreadTableViewController.h
//  EDMW Got-App-Got-Talk!
//
//  Created by A Jekanath on 8/8/14.
//  Copyright (c) 2014 LiteralDev Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TFHpple.h"

@interface EDMWThreadTableViewController : UITableViewController<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic)NSString *threadURL;
@property (strong,nonatomic)NSString *threadTitle;
@end
