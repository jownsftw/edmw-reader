
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// MMProgressHUD
#define COCOAPODS_POD_AVAILABLE_MMProgressHUD
#define COCOAPODS_VERSION_MAJOR_MMProgressHUD 0
#define COCOAPODS_VERSION_MINOR_MMProgressHUD 2
#define COCOAPODS_VERSION_PATCH_MMProgressHUD 2

// REFrostedViewController
#define COCOAPODS_POD_AVAILABLE_REFrostedViewController
#define COCOAPODS_VERSION_MAJOR_REFrostedViewController 2
#define COCOAPODS_VERSION_MINOR_REFrostedViewController 4
#define COCOAPODS_VERSION_PATCH_REFrostedViewController 6

// hpple
#define COCOAPODS_POD_AVAILABLE_hpple
#define COCOAPODS_VERSION_MAJOR_hpple 0
#define COCOAPODS_VERSION_MINOR_hpple 2
#define COCOAPODS_VERSION_PATCH_hpple 0

